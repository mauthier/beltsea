
build_pbr_by_Fr <- function(param_list = list(depletion = 0.3, 
                                              mnpl = 0.5,
                                              mnp = 0.04
                                              ),
                            K = 1E4, 
                            Rmax = 0.04, 
                            CV = 0.2,
                            frequency = 6,
                            bycatch_variation_cv = 0.3,
                            bias_byc = 1,
                            distribution = "truncnorm",
                            q = 0.2,
                            recoverfactor2evaluate = seq(0.1, 1, 0.05),
                            k_trend = 1,
                            bias_rmax = 1,
                            bias_abund = 1,
                            catastrophe = 0
                            ) {
  
  
  # Pella-Tomlinson age-aggregated model
  set.seed(20220817)
  
  scenario <- pmap(
    param_list,
    function(depletion, mnpl, mnp) { 
      pellatomlinson_pbr(
        vital_param = vital_param_pbr(depletion0 = depletion, 
                                      MNPL = mnpl, 
                                      Rmax = mnp, 
                                      K = K
                                      ), 
        CV = CV, 
        verbose = FALSE
      ) 
    }
  )
  
  
  # forward depending on F_r
  tuning_Fr <- lapply(recoverfactor2evaluate,
                      function(f) {
                        lapply(scenario, 
                               forward_pbr, 
                               frequency = frequency,
                               bycatch_variation_cv = bycatch_variation_cv,
                               bias_byc = bias_byc,
                               distribution = distribution,
                               Rmax = Rmax,
                               F_r = f,
                               q = q,
                               bias_Rmax = bias_rmax,
                               bias_abund = bias_abund,
                               Ktrend = k_trend,
                               catastrophe = catastrophe
                               )
                      }
  )
  
  
  
  trajectory_Fr <- tuning_Fr %>%
    unlist(recursive =  F) %>% # unlist first level of list
    map("depletion") %>%     # get depletion data.frame
    map_df(bind_rows)
  
  return(trajectory_Fr)
  
}

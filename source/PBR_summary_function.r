### Only keep the final depletion
summary_simul <- function(tuning_files,
                          path_res,
                          operating_model,
                          target
                          ) {
  df_all_tuning_Fr <- tuning_files %>% 
    file.path(path_res, .) %>% 
    map_df(readRDS) %>%
    filter(time == 100) %>%
    left_join(all_combination %>% 
                select(-c(mnpl, q, f_r)), 
              by = "scenario"
              ) %>%
    # careful here
    mutate(ID = rep(1:2E4, 2*19)) %>%
    left_join(.,
              cbind(to_keep %>%
                      select(mnp, xlev, mnpl, ylev), 
                    pars %>%
                      select(-seed_id)
                      # rename(seed = seed_id)
                    ) %>%
                mutate(ID = 1:2E4), 
              by = "ID"
              # by = c("seed", "ID")
              )
  
  out <- data.frame(margin_x = c(TRUE, TRUE, FALSE, FALSE),
                    margin_y = c(TRUE, FALSE, TRUE, FALSE)
                    ) %>%
    pmap(.f = conservation_obj, 
         df = df_all_tuning_Fr, 
         target = target,
         model = operating_model
         )
  return(out)
}
### summarize results
conservation_obj <- function(df, 
                             target = 0.8, 
                             margin_x = FALSE,
                             margin_y = FALSE,
                             model
                             ) {
  
  varout <- c("scenario", "quantile", "recovery_factor", "model",
              "margins", "xlev", "ylev", "final_depletion", "proba", "n_sim"
              )
  
  ### complete summary
  if(!margin_x && !margin_y) {
    out <- df %>%
      group_by(scenario, quantile, recovery_factor) %>%
      mutate(proba = ifelse(depletion >= target, 1, 0)) %>%
      summarize(final_depletion = mean(depletion),
                proba = mean(proba),
                n_sim = n()
                ) %>%
      ungroup() %>%
      mutate(xlev = "all",
             ylev = "all",
             margins = "none"
             )
  }
  
  if(!margin_x && margin_y) {
    out <- df %>%
      group_by(scenario, quantile, recovery_factor, ylev) %>%
      mutate(proba = ifelse(depletion >= target, 1, 0)) %>%
      summarize(final_depletion = mean(depletion),
                proba = mean(proba),
                n_sim = n()
                ) %>%
      ungroup() %>%
      mutate(xlev = "all",
             margins = "y"
             )
  }
  
  if(margin_x && !margin_y) {
    out <- df %>%
      group_by(scenario, quantile, recovery_factor, xlev) %>%
      mutate(proba = ifelse(depletion >= target, 1, 0)) %>%
      summarize(final_depletion = mean(depletion),
                proba = mean(proba),
                n_sim = n()
                ) %>%
      ungroup() %>%
      mutate(ylev = "all",
             margins = "x"
             )
  }
  
  if(margin_x && margin_y) {
    out <- df %>%
      group_by(scenario, quantile, recovery_factor, xlev, ylev) %>%
      mutate(proba = ifelse(depletion >= 0.8, 1, 0)) %>%
      summarize(final_depletion = mean(depletion),
                proba = mean(proba),
                n_sim = n()
                ) %>%
      ungroup() %>%
      mutate(xlev = as.character(xlev),
             ylev = as.character(ylev),
             margins = "both"
             )
  }
  
  out %>%
    mutate(model = model) %>%
    select(all_of(varout)) %>% 
    return()
}

summary_plots <- function(result_list, 
                          target = 0.8,
                          path_save,
                          save_name
                          ) {
  theme_set(theme_bw())
  
  plot1 <- result_list[[1]] %>%
    filter(proba >= target) %>%
    group_by(scenario, quantile, xlev, ylev) %>%
    arrange(recovery_factor) %>%
    summarize(recovery_factor = last(recovery_factor)) %>%
    ggplot(aes(x = xlev, y = ylev, fill = recovery_factor)) +
    geom_tile() +
    facet_wrap(~ scenario, ncol = 2) +
    xlab("True Intrinsic\nGrowth Rate") +
    ylab("True MNPL") +
    scale_fill_viridis_c(name = "Recovery factor", 
                         breaks = seq(0.1, 1.0, 0.1)
                         ) +
    theme(legend.position = "top",
          legend.text = element_text(angle = 45),
          axis.text.x = element_text(angle = 45)
          )
  
    ggsave(plot1,
           filename = paste(path_save, "/01_", save_name, "_MNPL_MNP.png", sep = ""), 
           dpi = 600, units = 'cm',
           width = 20, height = 15
           )
  
  ## true intrinsic growth rate
  plot2 <- result_list[[2]] %>%
    ggplot(aes(x = recovery_factor, y = proba, 
               group = xlev, color = xlev
               )
           ) +
    geom_line() +
    geom_hline(yintercept = 0.8, color = "tomato", linetype = "dashed") +
    facet_wrap(~ scenario, ncol = 2) +
    scale_x_continuous(name = quote(F[R]), breaks = seq(0.1, 1.0, 0.1)) +
    scale_y_sqrt(name = paste("Probability(depletion > ", 100 * target,
                              "% of K\nafter 100 years)", 
                              sep = ""
                              ),
                 breaks = seq(0.0, 1.0, 0.1)
                 ) +
    scale_colour_viridis_d(name = "True Intrinsic\nGrowth Rate") +
    theme(legend.position = "top")
  
    ggsave(plot2,
           filename = paste(path_save, "/02_", save_name, "_MNP.png", sep = ""), 
           dpi = 600, units = 'cm',
           width = 20, height = 15
           )
  
  ## true MNPL
  plot3 <- result_list[[3]] %>%
    ggplot(aes(x = recovery_factor, y = proba, 
               group = ylev, color = ylev
               )
           ) +
    geom_line() +
    geom_hline(yintercept = 0.8, color = "tomato", linetype = "dashed") +
    facet_wrap(~ scenario, ncol = 2) +
    scale_x_continuous(name = quote(F[R]), breaks = seq(0.1, 1.0, 0.1)) +
    scale_y_sqrt(name = paste("Probability(depletion > ", 100 * target,
                              "% of K\nafter 100 years)", 
                              sep = ""
                              ),
                 breaks = seq(0.0, 1.0, 0.1)
                 ) +
    scale_colour_viridis_d(name = "True MNPL") +
    theme(legend.position = "top")
  
    ggsave(plot3,
           filename = paste(path_save, "/03_", save_name, "_MNPL.png", sep = ""), 
           dpi = 600, units = 'cm',
           width = 20, height = 15
           )
  
  ## true MNPL
  plot4 <- result_list[[4]] %>%
    ggplot(aes(x = recovery_factor, y = proba)) +
    geom_line() +
    geom_hline(yintercept = 0.8, color = "tomato", linetype = "dashed") +
    facet_wrap(~ scenario, ncol = 2) +
    scale_x_continuous(name = quote(F[R]), breaks = seq(0.1, 1.0, 0.1)) +
    scale_y_sqrt(name = "Probability(depletion > 80% of K\nafter 100 years)",
                 breaks = seq(0.0, 1.0, 0.1)
                 ) +
    scale_colour_viridis_d(name = "True MNPL")
  
    ggsave(plot4,
           filename = paste(path_save, "/04_", save_name, ".png", sep = ""), 
           dpi = 600, units = 'cm',
           width = 20, height = 15
           )
}

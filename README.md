# beltsea

Additional simulations for determining a anthropogenic removals limit on Harbour Porpoises in the Belt Sea using the PBR control rule

## Attention conservation notice

The RLA package has currently being updated. The code used here will work on RLA v.0.1.7 and above.
The next version of the package (v.1.0.0) will break code compatibility with previous scripts because of some major changes (and clean-up) in arguments required in the main functions.

## R Scripts

All scripts have been tested except for scripts 'R/03_visualize_scenarios.R' and '07_visualize_scenarios_dis.R'. These *should* work but do not in practice due to the large number of simulations to plot. Attempt running those at your own risk (or use 'R/09_Subsample') which plots a subsample of simulations.

## Simulations results

Because these are large files, they are not stored on the gitlab but can be downloaded here:

<https://filesender.renater.fr/?s=download&token=92178d72-c7a3-4501-81d6-50f39bc5c2ee>

The link will expire on the 28th of September 2022. Please send me an email if you require a new link.


## Project status

This gitlab is currently public and hosted at La Rochelle University.

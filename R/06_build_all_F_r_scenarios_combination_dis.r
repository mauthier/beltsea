source("R/00_setup.R")
source("source/build_pbr_by_Fr_dis.R")

path_save <- "output/01_output_F_r_scenarios_combination"
if(!dir.exists(path_save)){
  dir.create(path_save, recursive = TRUE)
}

load("output/all_combination_specs.RData")
load("output/20220817_Priors.RData")
load("output/20220818_AgeDisaggregated.RData")

for(l in 1:nrow(all_combination)) {
  
  print(all_combination$scenario[l])
  
  tmp_combination <- NULL
  tmp_output_F_r <- NULL
  
  tmp_combination <- all_combination[l,]

  tmp_output_F_r <- build_pbr_by_Fr(param_list = list(mnpl = to_keep$mnpl, 
                                                      mnp = 1 + to_keep$mnp, 
                                                      r = pars$r, 
                                                      cv_byc = pars$cv_byc, 
                                                      cv_obs = pars$cv_obs, 
                                                      cv_env = pars$cv_env, 
                                                      seed_id = pars$seed_id
                                                      ),
                                    Rmax = tmp_combination$rmax, 
                                    CV = tmp_combination$cv,
                                    frequency = tmp_combination$frequency,
                                    bycatch_variation_cv = tmp_combination$bycatch_cv,
                                    bias_byc = tmp_combination$biased_bycatch,
                                    distribution = "truncnorm",
                                    q = 0.2,
                                    recoverfactor2evaluate = seq(0.1, 1, 0.05),
                                    bias_rmax = tmp_combination$biased_rmax,
                                    bias_abund = tmp_combination$biased_abundance,
                                    k_trend = tmp_combination$biased_abundance,
                                    catastrophe = tmp_combination$catastrophic_mortality_event
                                    ) %>% 
    mutate(scenario = tmp_combination$scenario)
  
  
  saveRDS(
    tmp_output_F_r, 
    file = file.path(path_save, 
                     paste0("scenario_", tmp_combination$scenario, "_output.rds")
                     )
  )
  rm(tmp_output_F_r)
  gc()
}




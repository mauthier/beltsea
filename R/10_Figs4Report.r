library(tidyverse)
library(ggdensity)

load("output/20220817_Priors.RData")
quantile(to_keep$mnp, probs = c(0.025, 0.975))
quantile(to_keep$mnpl, probs = c(0.025, 0.975))

n_hat <- 17301
cv <- 0.2
f_r <- c(0.20, 0.25, 0.40)
n_min <- n_hat / exp(qnorm(0.8) * sqrt(log1p(cv * cv)))
r_max <- 0.04

pbr <- (0.5 * r_max * n_min * f_r) %>%
  floor()

to_keep %>%
  mutate(mnp = 100 * mnp,
         mnpl = 100 * mnpl
         ) %>%
  ggplot(aes(x = mnp, y = mnpl)) +
  geom_hdr(aes(fill = after_stat(probs)), alpha = 1) +
  geom_hdr_rug(aes(fill = after_stat(probs)), length = unit(.2, "cm"), alpha = 1) +
  geom_vline(xintercept = 4, linetype = "dashed", color = "white", size = 1) +
  geom_hline(yintercept = 60, linetype = "dashed", color = "white", size = 1) +
  scale_x_continuous(name = "Maximum Net Productivity (%)",
                     breaks = 0:10
                     ) +
  scale_y_continuous(name = "Maximum Net Productivity Level (%)",
                     breaks = seq(40, 80, 5)
                     ) +
  scale_fill_viridis_d() + # begin = .8, end = 0
  ggtitle("Input values for MNP and MNPL") +
  theme_bw()
ggsave(filename = "output/MNP-MNPL.png", 
       width = 15, height = 12, unit = 'cm', dpi = 600
       )

### Fr
source("R/00_setup.R")

path_save <- "output"

### weight scenarios
## 0A and 0B likely (neutral)
## 1A and 1B not likely (half of 0AB)
## 1C and 1D very likely (twice 0AB)

w <- c(1, 1, 0.5, 0.5, 2, 2)

fr_results <- read_xlsx("data/Summary.xlsx", sheet = 1) %>% 
  as_tibble() %>% 
  mutate(F_r = ifelse(F_r == "X", NA, F_r),
         weights = rep(w / (2 * sum(w)), 2),
         # w_mean = F_r * weights 
         ) %>% 
  clean_names() %>% 
  mutate(across(q:catastrophic_mortality_event, as.numeric)) 

fr_results %>%
  pull(f_r) %>%
  median()

fr_results %>%
  mutate(w_mean = f_r * weights) %>%
  pull(w_mean) %>%
  sum()

fr_results %>%
  ggplot(aes(x = f_r)) +
  geom_histogram(breaks = seq(0, 0.6, 0.05)) +
  theme_bw()

source("R/00_setup.R")
source("source/PBR_summary_function.r")

# path save
path_save <- "output/02_graph_all_plot"
if(!(dir.exists(path_save))) {
  dir.create(path_save, recursive = TRUE)
}

### load the data
load("output/all_combination_specs.RData")
load("output/20220817_Priors.RData")
load("output/20220818_AgeDisaggregated.RData")

# path res
path_res <- paste("output/0", 0:1, "_output_F_r_scenarios_combination/", sep = "")
operating_model <- c("age-aggregated", "age-disaggregated")

for(i in path_res) {
  writeLines(paste0("\t", i))
  all_tuning_files <- list.files(i, pattern = ".rds")
  
  res <- data.frame(index = 1:length(all_tuning_files),
                    trial = str_sub(all_tuning_files, start = 10, end = 10),
                    scenario = str_sub(all_tuning_files, start = 11, end = 11),
                    model = operating_model[as.numeric(str_sub(i, start = 9, end = 9)) + 1]
                    )
  
  for(j in 1:(nrow(res) / 2)) {
    writeLines(paste0("\t\t", j))
    # temporary variables
    what <- 2 * (j - 1) + 1:2
    current_lines <- res %>%
      filter(index %in% what)
    name_for_saving <- paste("Scenario",
                             current_lines %>% 
                               pull(trial) %>% 
                               unique(),
                             current_lines %>% 
                               pull(scenario) %>% 
                               paste(., collapse = ""),
                             "_",
                             current_lines %>% 
                               pull(model) %>% 
                               unique(),
                             sep = ""
                             )

    ### get the summary results
    all_results <- summary_simul(tuning_files = all_tuning_files[what],
                                 path_res = i,
                                 operating_model = current_lines %>% 
                                   pull(model) %>% 
                                   unique(),
                                 target = 0.8
                                 )
    gc()
    
    ### Some plots
    summary_plots(result_list = all_results, 
                  target = 0.8,
                  path_save = path_save,
                  save_name = name_for_saving
                  )
    
    ## save the results
    do.call('rbind', all_results) %>%
      write.table(file = paste("output/", name_for_saving, ".txt", sep = ""),
                  row.names = FALSE, col.names = TRUE, sep = '\t'
                  )
    rm(what, current_lines, name_for_saving)
  }; rm(j, all_tuning_files)
}; rm(i)
gc()

## all results
all_results <- map_dfr(.x = c("output/Scenario0AB_age-aggregated.txt",
                              "output/Scenario1AB_age-aggregated.txt",
                              "output/Scenario1CD_age-aggregated.txt",
                              "output/Scenario0AB_age-disaggregated.txt",
                              "output/Scenario1AB_age-disaggregated.txt",
                              "output/Scenario1CD_age-disaggregated.txt"
                              ),
                       .f = function(x) { read.table(file = x, header = TRUE) }
                       )

theme_set(theme_bw())
all_results %>%
  filter(margins == "none") %>%
  ggplot(aes(x = recovery_factor, y = proba, 
             group = model, color = model
             )
         ) +
  geom_line() +
  geom_hline(yintercept = 0.8, color = "tomato", linetype = "dashed") +
  facet_wrap(~ scenario, ncol = 2) +
  scale_x_continuous(name = quote(F[R]), breaks = seq(0.1, 1.0, 0.1)) +
  scale_y_continuous(name = "Probability(depletion > 80% of K\nafter 100 years)",
                     breaks = seq(0.0, 1.0, 0.1)
                     ) +
  scale_colour_viridis_d(name = "Operating model") +
  theme(legend.position = "bottom")

ggsave(filename = "output/AllScenarios.png", 
       dpi = 600, units = 'cm',
       width = 20, height = 15
       )

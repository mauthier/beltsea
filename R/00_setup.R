# get needed package

# cran packages
pkg <- c("tidyverse",
         "sf",
         "purrr",
         "forcats",
         "readxl",
         "janitor",
         "kableExtra",
         "glue",
         "patchwork",
         "RColorBrewer",
         "ggrepel"
         )

uninstalled_pkg <- pkg %in% installed.packages()

lapply(pkg[!uninstalled_pkg], install.packages)
invisible(lapply(pkg, function(x)library(x, character.only = TRUE, quietly = TRUE)))


# github packages
gh_pkg <- c(NULL)
gh_repo <- c(NULL)
uninstalled_gh_pkg <- gh_pkg %in% installed.packages()

lapply(gh_repo[!uninstalled_gh_pkg], remotes::install_github)

invisible(lapply(gh_pkg, function(x)library(x, character.only = TRUE, quietly = TRUE)))

rm(list = ls())


# gitlab packages
gl_pkg <- c("RLA")
gl_repo <- c("pelaverse/RLA")
gl_host <- c("gitlab.univ-lr.fr")
uninstalled_gl_pkg <- gl_pkg %in% installed.packages()

if(gl_pkg[!uninstalled_gl_pkg] %>% length() > 0 ) {
  map2(.x = gl_repo[!uninstalled_gl_pkg],
       .y = gl_host[!uninstalled_gl_pkg],
       ~remotes::install_gitlab(repo = .x, host = .y)
       )
}


invisible(lapply(gl_pkg, function(x)library(x, character.only = TRUE, quietly = TRUE)))

rm(list = ls())

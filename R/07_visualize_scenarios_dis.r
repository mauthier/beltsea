source("R/00_setup.R")
source("source/PBR_graph_function.R")

# path save
path_save <- "output/02_graph_all_plot"
if(!(dir.exists(path_save))) {
  dir.create(path_save, recursive = T)
}

# path res
path_res <- "output/01_output_F_r_scenarios_combination/"

all_tuning_files <- list.files(path_res, pattern = ".rds")

df_all_tuning_Fr <- all_tuning_files[1:2] %>% 
  file.path(path_res,.) %>% 
  map_df(readRDS)

# cbind scenar specs
load("output/all_combination_specs.RData")

df_all_tuning_Fr <- df_all_tuning_Fr %>% 
  left_join(all_combination 
            %>% select(-c(q, f_r)), 
            by = "scenario"
            )

saveRDS(df_all_tuning_Fr, "res/PBR/df_all_tuning_1000.rds")

# function to build tuning plot of scenario A & B + proba graph
build_assemble_plot <- function(data, scenario_input, path_save) {
  
  tmp_data_filtered_scenar <- data %>% 
    filter(scenario %in% scenario_input)
  
  tuning_scenar_plotA <- graph_Fr_tuning(data = data, scenario_input = scenario_input[1]) +
    scale_color_distiller(palette = "Reds") +
    ggtitle(glue("{scenario_input[1]} (cv = {tmp_data_filtered_scenar$cv %>%  min()})")) +
    theme(text = element_text(size=20))
  tuning_scenar_plotB <- graph_Fr_tuning(data = data, scenario_input = scenario_input[2]) + 
    scale_color_distiller(palette = "Blues") +
    ggtitle(glue("{scenario_input[2]} (cv = {tmp_data_filtered_scenar$cv %>%  max()})")) +
    theme(text = element_text(size=20))
  
  
  
  # proba viz
  
  last_time_values <- data %>%
    filter(time == 100) %>%                                # only keep last time dépletion
    mutate(objective = ifelse(depletion > 0.8, 1, 0)) %>% 
    group_by(quantile, recovery_factor, cv, scenario) %>% 
    summarize(proba = mean(objective), .groups = "drop")
  
  
  proba_line <- proba_line_graph(data = last_time_values, scenario_input = scenario_input) +
    scale_colour_manual(values = c(
      brewer.pal(3, "Reds")[3],
      brewer.pal(3, "Blues")[3]
    )) +
    theme(text = element_text(size=20))
  
  
  # # proba table
  # proba_table(last_time_values, scenario_input = scenario_input)
  # 
  
  # assemble plots
  all_plot <- ((tuning_scenar_plotA /  tuning_scenar_plotB) + plot_layout(guides = 'collect')) | proba_line  
  all_plot
  
  ggsave(plot = all_plot,
         file = file.path(path_save,paste0("all_plot_",paste(scenario_input,collapse ="_"),".png")),
         width = 14,
         height = 8
         )
}

scenario_to_plot <- all_combination$scenario %>% 
  str_extract("\\d+") %>% 
  split(all_combination$scenario, f = .)


scenario_to_plot[[1]] %>% 
  map(~build_assemble_plot(data = df_all_tuning_Fr, scenario_input = .,  path_save = path_save))

source("R/00_setup.R")

path_save <- "output"

fr_results <- read_xlsx("data/Summary.xlsx", sheet = 1) %>% 
  as_tibble() %>% 
  mutate(F_r = ifelse(F_r == "X", NA, F_r)) %>% 
  clean_names() %>% 
  mutate(across(q:catastrophic_mortality_event, as.numeric)) 

load("output/20220817_Priors.RData")
load("output/20220818_AgeDisaggregated.RData")

### sub-sample for plotting
# path res
path_res <- paste("output/0", 0:1, "_output_F_r_scenarios_combination/", sep = "")
operating_model <- c("age-aggregated", "age-disaggregated")
set.seed(20220829)
sample_id <- sample.int(2E4, size = 1e3)

for(i in path_res) {
  all_tuning_files <- list.files(i, pattern = ".rds")
  m <- operating_model[as.numeric(str_sub(i, start = 9, end = 9)) + 1]
  
  res <- data.frame(index = 1:length(all_tuning_files),
                    trial = str_sub(all_tuning_files, start = 10, end = 10),
                    scenario = str_sub(all_tuning_files, start = 11, end = 11),
                    model = operating_model[as.numeric(str_sub(i, start = 9, end = 9)) + 1]
                    )
  
  for(j in 1:length(all_tuning_files)) {
    selected_fr <- fr_results %>%
      filter(scenario == res %>%
               filter(index == j) %>%
               mutate(scenario = paste0(trial, scenario)) %>%
               pull(scenario),
             operating_model == m
             ) %>%
      pull(f_r) %>%
      as.numeric()
    
    all_simul <- all_tuning_files[j] %>% 
      file.path(i, .) %>% 
      readRDS() %>%
      # careful here
      mutate(ID = rep(1:2E4, each = 101, times = 19)) %>%
      left_join(.,
                cbind(to_keep %>%
                        select(mnp, xlev, mnpl, ylev), 
                      pars %>%
                        select(-seed_id)
                      ) %>%
                  mutate(ID = 1:2E4), 
                by = "ID"
                ) %>%
      mutate(recovery_factor = round(recovery_factor, 2)) %>%
      filter(recovery_factor == selected_fr) 
    
    name_for_saving <- fr_results %>%
      filter(scenario == res %>%
               filter(index == j) %>%
               mutate(scenario = paste0(trial, scenario)) %>%
               pull(scenario),
             operating_model == m
             ) %>%
      select(scenario, operating_model) %>%
      paste(., collapse = "_")
    
    all_simul %>%
      filter(ID %in% sample_id) %>%
      arrange(ID, time) %>%
      mutate(model = m) %>%
      write.table(file = paste("output/Subsample_", name_for_saving, ".txt", sep = ""),
                  row.names = FALSE, col.names = TRUE, sep = '\t'
                  )
    
    rm(name_for_saving, all_simul, selected_fr)
    
  }; rm(j)
}; rm(m, all_tuning_files, res, i)
gc()

## all results
all_results <- map_dfr(.x = c("output/Subsample_0A_age-aggregated.txt",
                              "output/Subsample_0B_age-aggregated.txt",
                              "output/Subsample_1A_age-aggregated.txt",
                              "output/Subsample_1B_age-aggregated.txt",
                              "output/Subsample_1C_age-aggregated.txt",
                              "output/Subsample_1D_age-aggregated.txt",
                              "output/Subsample_0A_age-disaggregated.txt",
                              "output/Subsample_0B_age-disaggregated.txt",
                              "output/Subsample_1A_age-disaggregated.txt",
                              "output/Subsample_1B_age-disaggregated.txt",
                              "output/Subsample_1C_age-disaggregated.txt",
                              "output/Subsample_1D_age-disaggregated.txt"
                              ),
                       .f = function(x) { read.table(file = x, header = TRUE) }
                       ) %>%
  group_by(model, scenario, ID) %>%
  arrange(time, .by_group = TRUE) %>%
  ungroup()

m <- ggplot() +
  geom_line(data = all_results,
            aes(x = time, y = depletion, group = ID),
            alpha = 0.025, color = "midnightblue"
            ) +
  geom_smooth(data = all_results,
              aes(x = time, y = depletion, method = "gam", se = FALSE),
              color = "black", linetype = "dotted"
              ) +
  geom_text(data = fr_results %>%
              rename(model = operating_model) %>%
              mutate(x = 10, y = 0.9,
                     recovery_factor = paste("Fr", f_r, sep = "=")
                     ),
            aes(x = x, y = y, label = recovery_factor)
            ) +
  geom_hline(yintercept = 0.8, color = "tomato", linetype = "dashed") +
  facet_grid(scenario ~ model) +
  scale_x_continuous(name = "Time", breaks = seq(0, 100, 25)) +
  scale_y_continuous(name = "Depletion (fraction of K)",
                     breaks = seq(0.0, 1.2, 0.20)
                     ) +
  theme_bw()

ggsave(m,
       filename = "output/Simulations_1000sample.png", 
       dpi = 600, units = 'cm',
       width = 20, height = 15
       )

helcom_plot <- function(trial) {
  m <- ggplot() +
    geom_line(data = all_results %>%
                filter(scenario == trial),
              aes(x = time, y = depletion, group = ID),
              alpha = 0.025, color = "midnightblue"
              ) +
    geom_smooth(data = all_results %>%
                  filter(scenario == trial),
                aes(x = time, y = depletion, method = "gam", se = FALSE),
                color = "black", linetype = "dotted"
                ) +
    geom_text(data = fr_results %>%
                filter(scenario == trial) %>%
                rename(model = operating_model) %>%
                mutate(x = 10, y = 1.1,
                       recovery_factor = paste("Fr", f_r, sep = "=")
                       ),
              aes(x = x, y = y, label = recovery_factor)
              ) +
    geom_hline(yintercept = 0.8, color = "tomato", linetype = "dashed") +
    facet_grid(scenario ~ model) +
    scale_x_continuous(name = "Time", breaks = seq(0, 100, 25)) +
    scale_y_continuous(name = "Depletion (fraction of K)",
                       breaks = seq(0.0, 1.2, 0.20)
                       ) +
    coord_cartesian(xlim = c(0, 100), ylim = c(0, 1.2)) +
    theme_bw()
  return(m)
}

ggsave(plot = helcom_plot(trial = "0A"),
       filename = "output/BLUES_0A.png", 
       dpi = 600, units = 'cm',
       width = 30, height = 15
       )

ggsave(plot = helcom_plot(trial = "1A"),
       filename = "output/BLUES_1A.png", 
       dpi = 600, units = 'cm',
       width = 30, height = 15
       )

ggsave(plot = helcom_plot(trial = "1C"),
       filename = "output/BLUES_1C.png", 
       dpi = 600, units = 'cm',
       width = 30, height = 15
       )

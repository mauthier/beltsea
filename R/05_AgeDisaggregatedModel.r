library(tidyverse)
library(RLA)

load("output/20220817_Priors.RData")

data("north_sea_hp")
str(north_sea_hp$life_history)

### life history parameters
belt_sea_hp <- north_sea_hp; rm(north_sea_hp)
belt_sea_hp$SCANS <- data.frame(year = c(1994, 2005, 2012, 2016, 2020),
                                N_hat = c(51660, 27901, 40475, 42324, 17301),
                                CVs = c(0.30, 0.39, 0.24, 0.30, 0.20)
                                )
belt_sea_hp$bycatch <- data.frame(year = NULL,
                                  lower = NULL,
                                  mean = NULL,
                                  upper = NULL
                                  )
belt_sea_hp$life_history$K <- 5e4
belt_sea_hp$life_history$sexratio <- c(1.0, 1.1)

set.seed(20220817)
pars <- data.frame(seed_id = NULL,
                   cv_byc = NULL,
                   cv_env = NULL,
                   cv_obs = NULL,
                   r = NULL
                   )
### restart at 14114
load("output/20220818_AgeDisaggregated.RData")
set.seed(20220818)
for(i in (nrow(pars) + 1):nrow(to_keep)) {
  prop <- data.frame(seed_id = sample.int(1e6, size = 1, replace = FALSE),
                     cv_byc = round(runif(1, 0.05, 0.50), 3),
                     cv_env = round(runif(1, 0.00, 0.20), 3),
                     cv_obs = round(runif(1, 0.15, 0.30), 3),
                     r = round(runif(1, 0.001, 0.050), 3)
                     )
  current_depletion <- pellatomlinson_rla(MNPL = to_keep$mnpl[i],
                                          K = belt_sea_hp$life_history$K,
                                          L = belt_sea_hp$life_history$L,
                                          eta = belt_sea_hp$life_history$eta,
                                          phi = belt_sea_hp$life_history$phi,
                                          m = belt_sea_hp$life_history$maturity,
                                          sexratio = belt_sea_hp$life_history$sexratio,
                                          MNP = 1+to_keep$mnp[i],
                                          # series of catches
                                          catches = bycatch_rw(n = 61, 
                                                               K = belt_sea_hp$life_history$K,
                                                               rate = prop$r, 
                                                               cv = prop$cv_byc, 
                                                               seed_id = prop$seed_id
                                                               ),
                                          CV = prop$cv_obs,
                                          CV_env = prop$cv_env,
                                          # mini-scans survey
                                          scans = c(46, 53, 57, 61),
                                          verbose = FALSE,
                                          # for reproducibility purposes
                                          seed_id = prop$seed_id
                                          )$depletion %>%
    last()
  while(current_depletion > 0.35 || current_depletion < 0.25) {
    prop <- data.frame(seed_id = sample.int(1e6, size = 1, replace = FALSE),
                       cv_byc = round(runif(1, 0.05, 0.50), 3),
                       cv_env = round(runif(1, 0.00, 0.20), 3),
                       cv_obs = round(runif(1, 0.15, 0.30), 3),
                       r = round(runif(1, 0.001, 0.050), 3)
                       )
    current_depletion <- pellatomlinson_rla(MNPL = to_keep$mnpl[i],
                                            K = belt_sea_hp$life_history$K,
                                            L = belt_sea_hp$life_history$L,
                                            eta = belt_sea_hp$life_history$eta,
                                            phi = belt_sea_hp$life_history$phi,
                                            m = belt_sea_hp$life_history$maturity,
                                            sexratio = belt_sea_hp$life_history$sexratio,
                                            MNP = 1+to_keep$mnp[i],
                                            # series of catches
                                            catches = bycatch_rw(n = 61, 
                                                                 K = belt_sea_hp$life_history$K,
                                                                 rate = prop$r, 
                                                                 cv = prop$cv_byc, 
                                                                 seed_id = prop$seed_id
                                                                 ),
                                            CV = prop$cv_obs,
                                            CV_env = prop$cv_env,
                                            # mini-scans survey
                                            scans = c(46, 53, 57, 61),
                                            verbose = FALSE,
                                            # for reproducibility purposes
                                            seed_id = prop$seed_id
                                            )$depletion %>%
      last()
  }
  pars <- rbind(pars, prop)
}

save(list = c("pars"), file = "output/20220819_AgeDisaggregated.RData")


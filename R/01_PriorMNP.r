library(tidyverse)

n_sim <- 1e6
### values found with Parameter solver (https://biostatistics.mdanderson.org/SoftwareDownload/SingleSoftware/Index/6)
## check the gamma parametrization between the softwares
x <- rgamma(n_sim, shape = 3.645, rate = 1 / (1.094E-2))

hist(100 * x, las = 1, bty = 'n', xlab = "Maximum Net Productivity",
     main = "Prior for maximum net productivity (%)"
     )
abline(v = 4, col = "midnightblue", lty = 2, lwd = 2)

mean(x)
sd(x)

round(quantile(x, probs = c(0.05, 0.1, 0.25, 0.5, 0.75, 0.9, 0.95)), 3)

y <- cut(x, breaks = seq(0, 0.1, 0.01))
summary(y)
round(table(y) / (n_sim - sum(is.na(y))), 3)

# check
sum(c(0.03, 0.14, 0.21, 0.21, 0.16, 0.11, 0.07, 0.04, 0.02, 0.01))
# sum(c(0.16, 0.41, 0.28, 0.11, 0.04))

## nicer plot
data.frame(x = 100 * x) %>%
  ggplot(aes(x = x)) +
  geom_histogram(fill = "midnightblue", color = "white") +
  geom_vline(xintercept = 4, linetype = "dashed", color = "tomato", size = 2) +
  scale_x_continuous(name = "maximum net productivity (%)",
                     breaks = seq(0, 15, 2.5)
                     ) +
  scale_y_continuous(name = "", breaks = NULL) +
  ggtitle("Prior values for MNP (average = 4%)") +
  theme_bw()


### prior on MNPL
mnpl <- 0.6 + 0.05 * rnorm(n_sim)
z <- cut(mnpl, breaks = c(0.45, 0.50, 0.55, 0.60, 0.65, 0.70, 0.75, 0.80))
summary(z)
round(table(z) / (n_sim - sum(is.na(z))), 3)

# check
sum(c(0.025, 0.13, 0.34, 0.34, 0.14, 0.02, 0.005))

data.frame(x = 100 * mnpl) %>%
  ggplot(aes(x = x)) +
  geom_histogram(fill = "midnightblue", color = "white") +
  geom_vline(xintercept = 60, linetype = "dashed", color = "tomato", size = 2) +
  scale_x_continuous(name = "maximum net productivity level (%)",
                     breaks = seq(40, 80, 5)
                     ) +
  scale_y_continuous(name = "", breaks = NULL) +
  ggtitle("Prior values for MNPL (average = 60%)") +
  theme_bw()


prop <- c(0.03, 0.14, 0.21, 0.21, 0.16, 0.11, 0.07, 0.04, 0.02, 0.01) %*% 
  t(c(0.025, 0.13, 0.34, 0.34, 0.14, 0.02, 0.005))
sum(prop)

dd <- data.frame(mnp = 100 * x,
           xlev = y,
           mnpl = 100 * mnpl,
           ylev = z
           ) %>%
  filter(!is.na(xlev),
         !is.na(ylev)
         ) %>%
  mutate(xfac = as.character(as.numeric(xlev)),
         yfac = as.character(as.numeric(ylev)),
         combi = paste(xfac, yfac, sep = "_"),
         combi = factor(combi, levels = do.call('c', lapply(1:10, function(x) {paste(x, 1:7, sep = "_")})))
         )

dd %>%
  pull(combi) %>%
  table()

n_keep <- 2e4
to_keep <- NULL
set.seed(20220809)
for(i in 1:nrow(prop)) {
  for(j in 1:ncol(prop)) {
    to_keep <- to_keep %>%
      rbind(.,
            dd %>%
              filter(combi ==  paste(i, j, sep = "_")) %>%
              slice_sample(n = round(n_keep * prop[i, j]))
            )
  }
}; rm(i, j)

to_keep %>%
  ggplot(aes(x = mnp, y = mnpl)) +
  # geom_point(color = "midnightblue", fill = NA, alpha = 0.1) +
  geom_density2d_filled() +
  geom_vline(xintercept = 4, linetype = "dashed", color = "white", size = 1) +
  geom_hline(yintercept = 60, linetype = "dashed", color = "white", size = 1) +
  scale_x_continuous(name = "maximum net productivity (%)",
                     breaks = seq(0, 15, 2.5)
                     ) +
  scale_y_continuous(name = "maximum net productivity level (%)",
                     breaks = seq(40, 80, 5)
                     ) +
  # scale_fill_viridis_c() +
  ggtitle("Prior values for MNP and MNPL") +
  theme_bw()
ggsave(filename = "MNP-MNPL.png", width = 15, height = 12, unit = 'cm', dpi = 600)

prop * n_keep
n_simul <- cbind(prop * n_keep, apply(prop * n_keep, 1, sum))
n_simul <- rbind(n_simul,
                 c(apply(prop * n_keep, 2, sum), n_keep)
                 )
# write.table(n_simul, file = "design.txt", sep = "\t",
#             col.names = FALSE, row.names = FALSE, quote = FALSE
#             )

to_keep <- to_keep %>%
  mutate(mnp = mnp / 1E2,
         mnpl = mnpl / 1E2
         )

save(list = c("to_keep"), file = "output/20220817_Priors.RData")
